# Custom filter

Custom filter module provides an ability to create some filter without writing
any module.

When we create a new Input format, we will be asked to choose the filters we
want. Some modules hook into filter module and provide additional filters here,
e.g. Textile, GeshiFilter, MathFilter. This module does the same thing, except
that it allows the users with the right permission to define their own filters.

How do we define our own filters?
First, we need to create a filter; a filter is a container for filter rules.
When we create/configure a new input format, every defined filter will appear in
the filters list.

A filter is only a container. We need to define some filter rules if we want it
to work. So, an input format contains one or more filters, each of those has its
own filter rules.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/customfilter).

For documentation and some examples of the module, visit the
[documentation page](http://drupal.org/node/210551).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/customfilter).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires the Drupal core module - Filter.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. The settings page is at admin/settings/customfilter.
   Non-administrator users need to have "administer customfilter" permission to
   use this.

Notes:
1. The PHP code used in the replacement rules must set the variable $result,
   which is then used from the module.
2. The variable $vars is an instance of stdClass(); therefore it cannot be used
   as an array.
3. The module defines a new constant that allows PHP code used in the
   replacement rules to know the version of the API implemented from the module.


## MAINTAINERS

 * Fernando Conceição (yukare) - https://www.drupal.org/u/yukare
 * Dominique CLAUSE (Dom.) - https://www.drupal.org/u/dom
 * Merlin Axel Rutz (geek-merlin) - https://www.drupal.org/u/geek-merlin
 * Zhang Terry (zterry95) - https://www.drupal.org/u/zterry95
 * arhip (arhip) - https://www.drupal.org/user/165321
 * Kenzi Stewart (IncrediblyKenzi) - https://www.drupal.org/u/incrediblykenzi
