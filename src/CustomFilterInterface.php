<?php

namespace Drupal\customfilter;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a customfilter entity.
 */
interface CustomFilterInterface extends ConfigEntityInterface {
  // Add get/set methods for your configuration properties here.
}
