<?php

namespace Drupal\customfilter\Form;

use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\customfilter\CustomFilterValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Builds the form to delete a Custom Filter.
 */
class CustomFilterDeleteForm extends EntityConfirmFormBase {

  /**
   * Custom filter validator.
   *
   * @var \Drupal\customfilter\CustomFilterValidator
   */
  protected $customFilterValidator;

  /**
   * Constructs a ContentEntityForm object.
   *
   * @param \Drupal\customfilter\CustomFilterValidator $custom_filter_validator
   *   Custom filter validator.
   */
  public function __construct(CustomFilterValidator $custom_filter_validator) {
    $this->customFilterValidator = $custom_filter_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('customfilter.validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);

    if ($warning = $this->customFilterValidator->validateFilter($this->entity)) {
      $form['warning'] = [
        '#prefix' => '<div>',
        '#markup' => $warning,
        '#suffix' => '</div>',
      ];
      $form['validation'] = [
        '#prefix' => '<div>',
        '#markup' => $this->t("Continue the operation to delete the filter and it's usage in text formats."),
        '#suffix' => '</div>',
      ];
      $form['description']['#prefix'] = '<div><br/>';
      $form['description']['#suffix'] = '</div>';
      $form['description']['#weight'] = 10;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete %name?', ['%name' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.customfilter.list');
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->entity->delete();
    $this->messenger()->addStatus($this->t('Filter %label has been deleted.', ['%label' => $this->entity->label()]));
    $form_state->setRedirect('entity.customfilter.list');
  }

}
