<?php

namespace Drupal\customfilter\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\customfilter\Entity\CustomFilter;

/**
 * Builds the form to add/edit a Custom Filter.
 */
class CustomFilterForm extends EntityForm {

  use RedirectDestinationTrait;

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);
    /** @var \Drupal\customfilter\Entity\CustomFilter $filter */
    $filter = $this->entity;

    $form['name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $filter->label(),
      '#description' => $this->t("Label for the filter."),
      '#required' => TRUE,
    ];
    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $filter->id(),
      '#description' => $this->t('The machine-readable name of the filter. This name
         must contain only lowercase letters, numbers, and underscores and
         can not be changed latter'),
      '#machine_name' => [
        'exists' => [CustomFilter::class, 'load'],
        'source' => ['name'],
      ],
      '#disabled' => !$filter->isNew(),
      '#required' => TRUE,
    ];

    $form['disable_cache'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Force disable cache'),
      '#description' => $this->t("BEWARE: you probably don't want to uncheck this. If you do, the filter result will not be cached and will rebuild everytime the content is displayed."),
      '#default_value' => !$filter->getCache(),
    ];

    $form['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#description' => $this->t('A description of the purpose of this filter.'),
      '#default_value' => $filter->getDescription(),
    ];

    $form['shorttip'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tips (short)'),
      '#default_value' => $filter->getShorttip(),
      '#rows' => 5,
      '#description' => $this->t('This tip will be show in edit content/comments forms.'),
    ];

    $form['longtip'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Tips (full)'),
      '#default_value' => $filter->getLongtip(),
      '#rows' => 20,
    ];

    $form['rules'] = [
      '#type' => 'value',
      '#value' => $filter->rules,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function copyFormValuesToEntity(EntityInterface $entity, array $form, FormStateInterface $form_state) {
    parent::copyFormValuesToEntity($entity, $form, $form_state);
    // Force cache disable.
    $entity->set('cache', !$form_state->getValue('disable_cache'));
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $filter = $this->entity;
    $status = $filter->save();
    if ($status) {
      $this->messenger()->addStatus($this->t('Saved the %label Custom Filter.', [
        '%label' => $filter->label(),
      ]));
    }
    else {
      $this->messenger()->addStatus($this->t('The %label Custom Filter was not saved.', [
        '%label' => $filter->label(),
      ]));
    }

    $form_state->setRedirect('entity.customfilter.list');
  }

  /**
   * {@inheritdoc}
   */
  public function delete(array $form, FormStateInterface $form_state) {
    $destination = [];
    $request = $this->getRequest();
    if ($request->query->has('destination')) {
      $destination = $this->getDestinationArray();
      $request->query->remove('destination');
    }

    $form_state->setRedirect('entity.customfilter.delete_form', [$this->entity->id()], ['query' => $destination]);
  }

}
