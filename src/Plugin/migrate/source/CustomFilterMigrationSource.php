<?php

namespace Drupal\customfilter\Plugin\migrate\source;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Plugin\migrate\source\DrupalSqlBase;

/**
 * Migration source for Custom Filter entities.
 *
 * @MigrateSource(
 *   id = "customfilter_migration_source",
 *   source_module = "customfilter"
 * )
 */
class CustomFilterMigrationSource extends DrupalSqlBase {

  /**
   * {@inheritdoc}
   */
  public function fields() {
    return [
      'fid' => $this->t('The custom filter internal ID'),
      'type' => $this->t('The custom filter machine name'),
      'name' => $this->t('The custom filter human name'),
      'description' => $this->t('Description'),
      'shorttip' => $this->t('Short tip'),
      'longtip' => $this->t('Long tip'),
      'cache' => $this->t('Cacheability'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'fid' => ['type' => 'integer'],
      'type' => [
        'type' => 'string',
        'max_length' => 64,
        'is_ascii' => TRUE,
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function query() {
    $query = $this->select('customfilter_filter', 'f');
    $query->fields('f', [
      'fid', 'type', 'name', 'cache', 'description', 'shorttip', 'longtip',
    ]);
    $query->orderBy('f.fid');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    $result = parent::prepareRow($row);

    // Add associated data from the rules table.
    $query = $this->select('customfilter_rule', 'r');
    $query->join('customfilter_filter', 'f', 'f.fid = r .fid');
    $query->fields('r', [
      'rid', 'fid', 'prid', 'name', 'description', 'enabled', 'matches', 'pattern', 'replacement', 'code', 'weight',
    ]);
    $query->fields('f', ['type']);
    $query->condition('r.fid', $row->getSourceProperty('fid'));
    $rules = $query->execute()->fetchAllAssoc('rid');
    foreach ($rules as &$rule) {
      /* Prefix rid with 'rule_' to match how filters are prefixed. */
      $rule['rid'] = 'rule_' . $rule['rid'];
      /* Fix prid to mimic the rid prefixing. Also prid 0 is now ''. */
      $rule['prid'] = $rule['prid'] ? 'rule_' . $rule['prid'] : '';
    }
    $row->setSourceProperty('rules', $rules);

    return $result;
  }

}
