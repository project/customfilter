<?php

/**
 * @file
 * A database agnostic dump for testing purposes.
 */

use Drupal\Core\Database\Database;

$connection = Database::getConnection();

$connection->schema()->createTable('customfilter_filter', [
  'description' => 'The table for filters.',
  'fields' => [
    'fid' => [
      'description' => 'The filter ID.',
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 1,
    ],
    'type' => [
      'description' => 'The machine-readable name of this filter.',
      'type' => 'varchar',
      'length' => 32,
      'not null' => TRUE,
      'default' => '',
    ],
    'name' => [
      'description' => 'The human-readable name of this filter..',
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => '',
    ],
    'cache' => [
      'description' => "A boolean value that is set when the filter result is cached.",
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 1,
    ],
    'description' => [
      'description' => 'The filter description.',
      'type' => 'text',
    ],
    'shorttip' => [
      'description' => 'The short description shown in the node editing page.',
      'type' => 'text',
    ],
    'longtip' => [
      'description' => 'The filter description shown in the filters description page.',
      'type' => 'text',
    ],
  ],
  'primary key' => ['fid'],
  'mysql_character_set' => 'utf8',
]);

$connection->insert('customfilter_filter')
  ->fields([
    'fid',
    'type',
    'name',
    'description',
    'cache',
    'shorttip',
    'longtip',
  ])
  ->values([
    'fid' => '1',
    'type' => 'filter1',
    'name' => 'Filter #1 name',
    'description' => 'Filter #1 description',
    'cache' => 1,
    'shorttip' => 'Filter #1 shorttip',
    'longtip' => 'Filter #1 longtip',
  ])
  ->values([
// On purpose.
    'fid' => '3',
    'type' => 'filter2',
    'name' => 'Filter #2 name',
    'description' => 'Filter #2 description',
    'cache' => 0,
    'shorttip' => 'Filter #2 shorttip',
    'longtip' => 'Filter #2 longtip',
  ])
  ->execute();

$connection->schema()->createTable('customfilter_rule', [
  'description' => 'The table for the filter rules.',
  'fields' => [
    'rid' => [
      'description' => 'The rule ID',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 1,
    ],
    'fid' => [
      'description' => 'The ID of the filter containing the replacement rule.',
      'type' => 'int',
      'size' => 'tiny',
      'not null' => TRUE,
      'default' => 0,
    ],
    'prid' => [
      'description' => 'The parent rule.',
      'type' => 'int',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ],
    'name' => [
      'description' => 'The name of the replacement rule.',
      'type' => 'varchar',
      'length' => 64,
      'not null' => TRUE,
      'default' => '',
    ],
    'description' => [
      'description' => 'The description of the rule.',
      'type' => 'text',
    ],
    'enabled' => [
      'description' => 'A boolean value that is set when the rule is enabled.',
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 1,
    ],
    'matches' => [
      'description' => 'The n-th matched string to replace.',
      'type' => 'int',
      'size' => 'small',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 1,
    ],
    'pattern' => [
      'description' => 'The regular expression.',
      'type' => 'text',
    ],
    'replacement' => [
      'description' => 'The replacement text.',
      'type' => 'text',
    ],
    'code' => [
      'description' => 'A boolean value that is set when the replacement text is PHP code to execute.',
      'type' => 'int',
      'size' => 'tiny',
      'unsigned' => TRUE,
      'not null' => TRUE,
      'default' => 0,
    ],
    'weight' => [
      'description' => 'The rule weight.',
      'type' => 'int',
      'size' => 'small',
      'unsigned' => FALSE,
      'not null' => TRUE,
      'default' => 0,
    ],
  ],
  'indexes' => [
    'customfilter_rule_fid' => ['fid'],
    'customfilter_rule_prid' => ['prid'],
    'customfilter_rule_weight' => ['weight'],
  ],
  'primary key' => ['rid'],
]);

$connection->insert('customfilter_rule')
  ->fields([
    'rid',
    'fid',
    'prid',
    'name',
    'description',
    'enabled',
    'matches',
    'pattern',
    'replacement',
    'code',
    'weight',
  ])
  ->values([
    'rid' => 1,
    'fid' => 1,
    'prid' => 0,
    'name' => 'Rule #1 name',
    'description' => 'Rule #1 description',
    'enabled' => 1,
    'matches' => 1,
    'pattern' => '/\[test\](.+)\[\/test\]/iUs',
    'replacement' => 'Rule #1 replacement',
    'code' => 0,
    'weight' => 1,
  ])
  ->values([
    'rid' => 2,
    'fid' => 1,
    'prid' => 0,
    'name' => 'Rule #2 name',
    'description' => 'Rule #2 description',
    'enabled' => 0,
    'matches' => 1,
    'pattern' => '/\[rule\](.+)\[\/rule\]/iUs',
    'replacement' => 'Rule #2 replacement',
    'code' => 1,
    'weight' => 2,
  ])
  ->values([
    'rid' => 3,
// Wrong on purpose.
    'fid' => 2,
    'prid' => 0,
    'name' => 'Rule #3 name',
    'description' => 'Rule #3 description',
    'enabled' => 0,
    'matches' => 1,
    'pattern' => '/\[rule\](.+)\[\/rule\]/iUs',
    'replacement' => 'Rule #3 replacement',
    'code' => 1,
    'weight' => 2,
  ])
  ->values([
// On purpose.
    'rid' => 5,
    'fid' => 3,
    'prid' => 0,
    'name' => 'Rule #4 name',
    'description' => 'Rule #4 description',
    'enabled' => 1,
    'matches' => 1,
    'pattern' => '/rule/iUs',
    'replacement' => 'Rule #4 replacement',
    'code' => 1,
    'weight' => 2,
  ])
  ->values([
    'rid' => 6,
    'fid' => 3,
    'prid' => 5,
    'name' => 'Subrule #1 name',
    'description' => 'Subrule #1 description',
    'enabled' => 0,
    'matches' => 1,
    'pattern' => '/subrule/iUs',
    'replacement' => 'Subrule #1 replacement',
    'code' => 1,
    'weight' => 2,
  ])
  ->execute();

$connection->insert('system')
  ->fields([
    'filename',
    'name',
    'type',
    'owner',
    'status',
    'bootstrap',
    'schema_version',
    'weight',
    'info',
  ])
  ->values([
    'filename' => 'sites/all/modules/customfilter/customfilter.module',
    'name' => 'customfilter',
    'type' => 'module',
    'owner' => '',
    'status' => '1',
    'bootstrap' => '0',
    'schema_version' => '6119',
    'weight' => '0',
    'info' => 'a:12:{s:4:"name";s:13:"Custom filter";s:11:"description";s:68:"Allows the users with the right permission to create custom filters.";s:7:"package";s:7:"Filters";s:4:"core";s:3:"7.x";s:5:"files";a:2:{i:0;s:20:"customfilter.install";i:1;s:19:"customfilter.module";}s:7:"version";s:7:"7.x-1.0";s:7:"project";s:12:"customfilter";s:9:"datestamp";s:10:"1294453551";s:5:"mtime";i:1600352672;s:12:"dependencies";a:0:{}s:3:"php";s:5:"5.2.4";s:9:"bootstrap";i:0;}',
  ])
  ->execute();
