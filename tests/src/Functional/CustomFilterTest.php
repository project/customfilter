<?php

namespace Drupal\Tests\customfilter\Functional;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\customfilter\Entity\CustomFilter;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\RoleInterface;

/**
 * Test the Custom Filter administration and use.
 *
 * @group customfilter
 */
class CustomFilterTest extends BrowserTestBase {

  /**
   * User with administrative permission on customfilter.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stable';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['filter', 'node', 'customfilter'];

  /**
   * Set up the tests and create the users.
   */
  public function setUp(): void {
    parent::setUp();

    // Create a content type, as we will create nodes on test.
    $settings = [
      // Override default type (a random name).
      'type' => 'test',
      'name' => 'Test',
    ];

    $this->drupalCreateContentType($settings);

    // Create a user with administrative privilegies.
    $this->adminUser = $this->drupalCreateUser([
      'administer permissions', 'administer customfilter',
      'administer filters', 'administer nodes', 'access administration pages',
      'create test content', 'edit any test content',
    ]);

    $this->drupalLogin($this->adminUser);
  }

  /**
   * Create a new filter.
   */
  protected function createFilter() {
    $edit = [];
    $edit['id'] = 'test_filter';
    $edit['name'] = 'filter_name';
    $edit['disable_cache'] = FALSE;
    $edit['description'] = 'filter description';
    $edit['shorttip'] = 'short tip';
    $edit['longtip'] = 'long tip';
    $this->drupalGet('admin/config/content/customfilter/add');
    $this->submitForm($edit, 'Save');
    $this->drupalGet('admin/config/content/customfilter');
    $this->assertSession()->pageTextContains('filter_name');
    $this->assertTrue(CustomFilter::load('test_filter')->getCache());
  }

  /**
   * Create a new rule.
   */
  protected function createRule() {
    $edit = [];
    $edit['rid'] = 'test_rule';
    $edit['name'] = 'Rule #1 Name';
    $edit['description'] = 'rule description';
    $edit['enabled'] = TRUE;
    $edit['pattern'] = '/\[test\](.+)\[\/test\]/iUs';
    $edit['code'] = FALSE;
    $edit['replacement'] = '<b>$1</b>';
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter/add');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Rule #1 Name');
  }

  /**
   * Create a new rule.
   */
  protected function createTimeRule() {
    $edit = [];
    $edit['rid'] = 'time_rule';
    $edit['name'] = 'Rule #2 Name';
    $edit['description'] = 'rule description';
    $edit['enabled'] = TRUE;
    $edit['pattern'] = '/\[time\]/iUs';
    $edit['code'] = TRUE;
    $edit['replacement'] = '$result = "<time>" . time () . "</time>";';
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter/add');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Rule #2 Name');
  }

  /**
   * Create a new subrule.
   */
  protected function createSubRule() {
    $edit = [];
    $edit['rid'] = 'test_subrule';
    $edit['name'] = 'Subrule Name';
    $edit['description'] = 'subrule description';
    $edit['enabled'] = TRUE;
    $edit['matches'] = '1';
    $edit['pattern'] = '/drupal/i';
    $edit['code'] = FALSE;
    $edit['replacement'] = '<font color="red">Drupal</font>';
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter/rule/test_rule/add');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains("Subrule Name");
  }

  /**
   * Create a new text format.
   */
  protected function createTextFormat() {
    $edit = [];
    $edit['format'] = 'customfilter';
    $edit['name'] = $this->randomMachineName();
    $edit['roles[' . RoleInterface::AUTHENTICATED_ID . ']'] = 1;
    $edit['filters[customfilter_test_filter][status]'] = TRUE;
    $this->drupalGet('admin/config/content/formats/add');
    $this->assertSession()->pageTextContains('filter_name');
    $this->submitForm($edit, 'Save configuration');
    $message = new FormattableMarkup('Added text format %filter.', ['%filter' => $edit['name']]);
    $this->assertSession()->responseContains($message);
    $this->drupalGet('admin/config/content/formats');
  }

  /**
   * Delete a filter.
   */
  protected function deleteFilter() {
    $edit = [];
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter/delete');
    $this->submitForm($edit, 'Delete');
    // Verify if filter has been removed from custom text format.
    $this->drupalGet('admin/config/content/formats/manage/customfilter');
    $this->assertSession()->pageTextNotContains('filter_name');
  }

  /**
   * Delete a subrule.
   */
  protected function deleteSubRule() {
    $edit = [];
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter/rule/test_rule/delete');
    $this->submitForm($edit, 'Delete');
  }

  /**
   * Edit an existing rule.
   */
  protected function editRule() {
    $edit = [];
    $edit['name'] = 'New rule label';
    $edit['description'] = 'rule description';
    $edit['enabled'] = TRUE;
    $edit['pattern'] = '/Goodbye Drupal 7/i';
    $edit['code'] = FALSE;
    $edit['replacement'] = 'Come back Drupal 7';
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter/rule/test_rule/edit');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->addressEquals('admin/config/content/customfilter/filter/test_filter');
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter');
    // Test if there is a rule with new name.
    $this->assertSession()->responseContains('New rule label');
    // Test if do not exist a rule with previous name, so the rule is edited
    // and not inserted as a new one.
    $this->assertSession()->responseNotContains('Rule #1 Name');
  }

  /**
   * Edit a existing subrule.
   */
  protected function editSubRule() {
    $edit = [];
    $edit['name'] = 'Renamed Subrule';
    $edit['description'] = 'New subrule description';
    $edit['matches'] = 1;
    $edit['pattern'] = '/Drupal/i';
    $edit['code'] = FALSE;
    $edit['replacement'] = '<font color="blue">Drupal</font>';
    $this->drupalGet('admin/config/content/customfilter/filter/test_filter/rule/test_subrule/edit');
    $this->submitForm($edit, 'Save');
    $this->assertSession()->pageTextContains('Renamed Subrule');
    $this->assertSession()->addressEquals('admin/config/content/customfilter/filter/test_filter');
    // Test if there is a rule with new name.
    $this->assertSession()->responseContains('Renamed Subrule');
    // Test if do not exist a rule with previous name, so the rule is edited
    // and not inserted as a new one.
    $this->assertSession()->responseNotContains('Subrule Name');
  }

  /**
   * Test if node content is replaced by our rules.
   */
  protected function rulesFilteringTest() {
    // Test node replacement.
    $this->drupalGet('node/1');
    $this->assertSession()->responseContains('Drupal <b>Goodbye <font color="red">Drupal</font> 7</b>');
    $this->assertSession()->responseContains('<time>');
    $time = $this->xpath('//time');
    $this->assertCount(1, $time);
    $time = $time[0]->getText();
    $this->assertNotNull($time);

    // Reloading node should not change time since filter is cached.
    sleep(1);
    $this->drupalGet('node/1');
    $newtime = $this->xpath('//time')[0]->getText();
    $this->assertSame($time, $newtime);

    // Change filter caching and rule.
    CustomFilter::load('test_filter')->set('cache', FALSE)->updateRule([
      'rid' => 'test_subrule',
      'replacement' => '<font color="orange">Drupal</font>',
    ])->save();

    // Rule has changed so node should have too.
    sleep(1);
    $this->drupalGet('node/1');
    $this->assertSession()->responseContains('Drupal <b>Goodbye <font color="orange">Drupal</font> 7</b>');
    $this->assertSession()->responseContains('<time>');
    $time = $this->xpath('//time')[0]->getText();
    $this->assertNotNull($time);

    // Reloading node should not change time since filter is no more cached.
    sleep(1);
    $this->drupalGet('node/1');
    $newtime = $this->xpath('//time')[0]->getText();
    $this->assertNotSame($time, $newtime);
  }

  /**
   * Run all the tests.
   */
  public function testModule() {
    // Test create filter.
    $this->createFilter();

    // Test create rules.
    $this->createRule();
    $this->createTimeRule();

    // Create a subrule.
    $this->createSubRule();

    // Test create a new text format with your filter enabled.
    $this->createTextFormat();

    // Create a node.
    $node = [
      'title' => 'Test for Custom Filter',
      'body' => [
        [
          'value' => 'Drupal [test]Goodbye Drupal 7[/test] at [time]',
          'format' => 'customfilter',
        ],
      ],
      'type' => 'test',
    ];
    $this->drupalCreateNode($node);

    // Test the node content with the rule.
    $this->rulesFilteringTest();

    // Edit the rule.
    $this->editRule();

    // Edit the sub rule.
    $this->editSubRule();

    // Delete the sub rule.
    $this->deleteSubRule();

    // Delete the filter.
    $this->deleteFilter();
  }

  /**
   * Test if CustomFilterMigrationSource shows on admin/config.
   *
   * This test is for https://drupal.org/node/2143991.
   */
  public function testAdminPage() {
    $this->drupalGet('admin/config');
    // Assert for module.
    $this->assertSession()->responseContains('Custom Filter');
    // Assert for description.
    $this->assertSession()->responseContains('Create and manage your own custom filters.');
    $this->assertSession()->linkByHrefExists('admin/config/content/customfilter',
      0,
      'A link to custom filter is on the page.');
  }

}
