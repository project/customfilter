<?php

namespace Drupal\Tests\customfilter\Kernel\Migrate\d6;

use Drupal\Tests\customfilter\Traits\CustomFilterMigrationTrait;
use Drupal\Tests\migrate_drupal\Kernel\d6\MigrateDrupal6TestBase;

/**
 * Tests migration of per-entity data from CustomFilterMigrationSource-D6.
 *
 * @group customfilter
 */
class CustomFilterD6MigrationTest extends MigrateDrupal6TestBase {
  use CustomFilterMigrationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'customfilter',
  ];

}
