<?php

namespace Drupal\Tests\customfilter\Kernel\Migrate\d7;

use Drupal\Tests\customfilter\Traits\CustomFilterMigrationTrait;
use Drupal\Tests\migrate_drupal\Kernel\d7\MigrateDrupal7TestBase;

/**
 * Tests migration of per-entity data from CustomFilterMigrationSource-D7.
 *
 * @group customfilter
 */
class CustomFilterD7MigrationTest extends MigrateDrupal7TestBase {
  use CustomFilterMigrationTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'filter',
    'customfilter',
  ];

}
