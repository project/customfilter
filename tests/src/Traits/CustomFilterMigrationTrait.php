<?php

namespace Drupal\Tests\customfilter\Traits;

use Drupal\customfilter\Entity\CustomFilter;

/**
 * Trait to gather custom filter migration testing code.
 *
 * @group customfilter
 */
trait CustomFilterMigrationTrait {

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->loadFixture(__DIR__ . '/../../fixtures/customfilter_entities.php');

    $this->installConfig(static::$modules);
    $this->installEntitySchema('customfilter');

    $this->executeMigrations([
      'customfilter_migrate',
    ]);
  }

  /**
   * Test custom filter migration from Drupal 6 to 8.
   */
  public function testCustomFilter() {

    // Check filter #1.
    /** @var \Drupal\customfilter\Entity\CustomFilter $filter1 */
    $filter1 = CustomFilter::load('filter1');
    $this->assertTrue($filter1 instanceof CustomFilter);
    $this->assertEquals('filter1', $filter1->id());
    $this->assertEquals('Filter #1 name', $filter1->label());
    $this->assertEquals('Filter #1 description', $filter1->getDescription());
    $this->assertEquals(TRUE, $filter1->getCache());
    $this->assertEquals('Filter #1 shorttip', $filter1->getShorttip());
    $this->assertEquals('Filter #1 longtip', $filter1->getLongtip());
    $this->assertEquals(2, count($filter1->getRules()));

    // Check rule #1 (ie first rule of filter#1).
    $rule1 = $filter1->getRule('rule_1');
    $this->assertEquals([
    // On purpose cf customfilter_entities.php.
      'rid' => 'rule_1',
      'fid' => 'filter1',
      'prid' => '',
      'name' => 'Rule #1 name',
      'description' => 'Rule #1 description',
      'enabled' => TRUE,
      'matches' => 1,
      'pattern' => '/\[test\](.+)\[\/test\]/iUs',
      'replacement' => 'Rule #1 replacement',
      'code' => FALSE,
      'weight' => 1,
    ], $rule1);

    // Check rule #2.
    $rule2 = $filter1->getRule('rule_2');
    $this->assertEquals([
    // On purpose cf customfilter_entities.php.
      'rid' => 'rule_2',
      'fid' => 'filter1',
      'prid' => '',
      'name' => 'Rule #2 name',
      'description' => 'Rule #2 description',
      'enabled' => FALSE,
      'matches' => 1,
      'pattern' => '/\[rule\](.+)\[\/rule\]/iUs',
      'replacement' => 'Rule #2 replacement',
      'code' => TRUE,
      'weight' => 2,
    ], $rule2);

    // Check filter #2.
    /** @var \Drupal\customfilter\Entity\CustomFilter $filter2 */
    $filter2 = CustomFilter::load('filter2');
    $this->assertEquals('filter2', $filter2->id());
    $this->assertEquals('Filter #2 name', $filter2->label());
    $this->assertEquals('Filter #2 description', $filter2->getDescription());
    $this->assertEquals(FALSE, $filter2->getCache());
    $this->assertEquals('Filter #2 shorttip', $filter2->getShorttip());
    $this->assertEquals('Filter #2 longtip', $filter2->getLongtip());
    $this->assertEquals(1, count($filter2->getRules()));

    // Check rule #4 (ie first rule of filter#2).
    $rule4 = $filter2->getRule('rule_5');
    $this->assertEquals([
    // On purpose cf customfilter_entities.php.
      'rid' => 'rule_5',
      'fid' => 'filter2',
      'prid' => '',
      'name' => 'Rule #4 name',
      'description' => 'Rule #4 description',
      'enabled' => 1,
      'matches' => 1,
      'pattern' => '/rule/iUs',
      'replacement' => 'Rule #4 replacement',
      'code' => 1,
      'weight' => 2,
    ], $rule4);

    // Check subrule #1.
    $subrule = $filter2->getRule('rule_6');
    $this->assertEquals([
    // On purpose cf customfilter_entities.php.
      'rid' => 'rule_6',
      'fid' => 'filter2',
    // On purpose cf customfilter_entities.php.
      'prid' => 'rule_5',
      'name' => 'Subrule #1 name',
      'description' => 'Subrule #1 description',
      'enabled' => 0,
      'matches' => 1,
      'pattern' => '/subrule/iUs',
      'replacement' => 'Subrule #1 replacement',
      'code' => 1,
      'weight' => 2,
    ], $subrule);
  }

}
